<?php

/**
* Implements hook_rules_action_info().
*/
function rules_generate_string_action_info() {
  $defaults = array(
   'parameter' => array(
      'length' => array(
        'type' => 'integer',
        'label' => t('Length'),
        'save' => TRUE,
      ),
      'charset' => array(
        'type' => 'string',
        'label' => t('Character set'),
        'save' => TRUE,
      ),
    ),
    'group' => t('Data'),
  );

  $items['generate_string'] = $defaults + array(
    'label' => t('Generate a string'),
    'base' => 'rules_action_generate_string',
  );

  return $items;
}

/**
* Action: Generate a string.
*/
function rules_generate_string_action_() {
  $str = rules_generate_string_generate_random();
}


